package com.example.financask.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.financask.R
import com.example.financask.model.Transacao
import com.example.financask.ui.activity.adapter.ListaTransacoesAdapter
import kotlinx.android.synthetic.main.activity_lista_transacoes.*
import java.math.BigDecimal
import java.util.*

class ListaTransacoesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_transacoes)


        val lista  = listOf(Transacao(BigDecimal(20.5),"Comida", Calendar.getInstance()),
                            Transacao(BigDecimal(100.0),"Transporte", Calendar.getInstance()))


        lista_transacoes_listview.setAdapter(ListaTransacoesAdapter(lista,this))
    }
}